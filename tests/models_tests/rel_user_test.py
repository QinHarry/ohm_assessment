from tests import OhmTestCase
from models.rel_user import RelUser

class RelUserTest(OhmTestCase):
    def test_get_location_by_id(self):
        location1 = RelUser.get_location_by_id(0)
        location2 = RelUser.get_location_by_id(1)
        assert location1 == ""
        assert location2 == "EUROPE"