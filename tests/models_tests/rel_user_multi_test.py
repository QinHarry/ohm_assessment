from tests import OhmTestCase
from models.rel_user_multi import RelUserMulti

class RelUserMultiTest(OhmTestCase):

    def test_get_phone_numbers_by_id(self):
        numbers = RelUserMulti.get_phone_numbers_by_id(1)
        assert len(numbers) == 2