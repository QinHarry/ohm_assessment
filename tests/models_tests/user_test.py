from tests import OhmTestCase
from models.user import User


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_get_recent_users(self):
        recent_users = User.get_recent_five_users()
        assert len(recent_users) == 3
        assert recent_users[0]['name'] == 'Justin Bieber'
