from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User, RelUser, RelUserMulti


@app.route('/dashboard', methods=['GET'])
def dashboard():

    login_user(User.query.get(1))

    args = {
            'gift_card_eligible': True,
            'cashout_ok': True,
            'user_below_silver': current_user.is_below_tier('Silver'),
    }
    return render_template("dashboard.html", **args)

@app.route('/community', methods=['GET'])
def community():
    login_user(User.query.get(1))
    users = User.get_recent_five_users()
    for user in users:
        user['phone'] = RelUserMulti.get_phone_numbers_by_id(user['id'])
        user['location'] = RelUser.get_location_by_id(user['id'])
    args = {
        'gift_card_eligible': True,
        'cashout_ok': True,
        'user_below_silver': current_user.is_below_tier('Silver'),
        'users': users
    }
    return render_template("community.html", **args)

